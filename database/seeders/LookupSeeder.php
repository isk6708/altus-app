<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class LookupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('tbl_lookups')->insert([
            ['cat' => 'WHOUR','code' => '10','descr' => 'NORMAL HOUR'],
            ['cat' => 'WHOUR','code' => '11','descr' => 'OVERTIME'],
            ['cat' => 'STATE','code' => '01','descr' => 'JOHOR'],
            ['cat' => 'STATE','code' => '02','descr' => 'KEDAH'],
            ['cat' => 'STATE','code' => '03','descr' => 'KELANTAN'],
            ['cat' => 'STATE','code' => '04','descr' => 'MELAKA'],
            ['cat' => 'STATE','code' => '11','descr' => 'TERENGGANU'],
            ]);
    }
}
