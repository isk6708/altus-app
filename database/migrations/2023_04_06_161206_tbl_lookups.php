<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tbl_lookups', function (Blueprint $table) {
            $table->id();
            $table->string('cat',5)->nullable();
            $table->string('code',5)->nullable();
            $table->string('descr')->nullable();
            $table->boolean('active')->nullable();
            $table->string('param')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tbl_lookups');
    }
};
