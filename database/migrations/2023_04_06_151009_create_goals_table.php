<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('goals', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->char('cat', 5)->nullable();
            $table->integer('month')->nullable();
            $table->integer('year')->nullable();
            $table->string('goal_name')->nullable();
            $table->double('target',8,2)->nullable();
            $table->double('actual',8,2)->nullable();
            $table->double('score',8,2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('goals');
    }
};
