<?php

namespace App\Http\Controllers;

use App\Models\JobReporting;
use Illuminate\Http\Request;

class JobReportingController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(JobReporting $jobReporting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(JobReporting $jobReporting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, JobReporting $jobReporting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(JobReporting $jobReporting)
    {
        //
    }
}
