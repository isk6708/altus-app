<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Auth as Auth2;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $req){
        $email = $req->email; 
        $name = $req->name; 
        
        $users = User::where(function($q) use ($email,$name){
            if (isset($email)){
                $q = $q->where('email',$email);
            }  
    
            if (isset($name)){
                $q = $q->where('name','LIKE','%'.$name.'%');
            }   
        })->orderBy('name')->paginate(2);

        return view('user.list',compact('users','name','email'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $user = new User();
        return view('user._form',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'email'=>'email|required|unique:App\Models\User,email',
            'name'=>'required',
            'password' => 'min:6|required_with:confirm_password|same:confirm_password',
            'confirm_password'=>'min:6'
        ]);


        $user = new user();
        $user->password = Hash::make($request->password);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        // $user->create($request->except('password'));
        
        return redirect('/user');
    }

    /**
     * Display the specified resource.
     */
    public function changePassword()
    {
        $user = User::find(Auth::user()->id);
        
        return view('user._profile',compact('user'));
    }
    public function show(Request $request){
        dd('Show');
    }

    public function updateProfile(Request $request)
    {
        // dd(Auth2::attempt(['email' => $request->email, 'password' => $request->current_password]));
        $user = User::find(Auth::user()->id);

        $userEmail = User::where('email',$request->email)
        ->where('id',Auth::user()->id)->first();
        if (!isset($userEmail))
        {
            // dd('Here 1');
            if (strlen($request->password) > 0){
                $validated = $request->validate([
                    'email'=>'email|required|unique:App\Models\User,email',
                    'name'=>'required',
                    'current_password'=>'required',
                    'password' => 'min:6|required_with:confirm_password|same:confirm_password',
                    'confirm_password'=>'min:6'
                ]);

                if (Hash::check( $request->current_password, $user->password )) {                
                    if (strlen($request->password) <60){
                        $user->password = Hash::make($request->password);
                    }
                }else{
                    return redirect()->route('user.my.profile')->withErrors(['msg'=>'Wrong Current Password']);
                }

            }else{
                $validated = $request->validate([
                    'email'=>'email|required|unique:App\Models\User,email',
                    'name'=>'required',
                ]);
            }
            $user->name = $request->name;
            $user->email = $request->email;

        }else{
            
            if (strlen($request->password) > 0){
                $validated = $request->validate([
                    'name'=>'required',
                    'current_password'=>'required',
                    'password' => 'min:6|required_with:confirm_password|same:confirm_password',
                    'confirm_password'=>'min:6'
                ]);

                if (Hash::check( $request->current_password, $user->password )) {                
                    if (strlen($request->password) <60){
                        $user->password = Hash::make($request->password);
                    }
                }else{
                    return redirect()->route('user.my.profile')->withErrors(['msg'=>'Wrong Current Password']);
                }

            }else{
                $validated = $request->validate([
                    'name'=>'required',
                ]);

                $user->name = $request->name;
            }
            
        }
        
        if (isset($user)){
            $user->save();
        }
        
        return redirect()->route('user.my.profile');
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $user = User::find($id);
        // dd(strlen($user->password));
        return view('user._form',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $userEmail = User::where('email',$request->email)->first();
        if (!isset($userEmail))
        {
            $validated = $request->validate([
                'email'=>'email|required|unique:App\Models\User,email',
                'name'=>'required',
                'password' => 'min:6|required_with:confirm_password|same:confirm_password',
                'confirm_password'=>'min:6'
            ]);    
        }else{
            $validated = $request->validate([
                // 'email'=>'email|required|unique:App\Models\User,email,'.$request->email,
                'name'=>'required',
                'password' => 'min:6|required_with:confirm_password|same:confirm_password',
                'confirm_password'=>'min:6'
            ]);
        }
        


        $user = User::find($id);

        if (strlen($request->password) <60){
            $user->password = Hash::make($request->password);
        }        

        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        // $user->create($request->except('password'));
        
        return redirect('/user');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        User::find($id)->delete();
        return redirect('/user');
    }
}
